import React, { Component } from "react";
import TodoItem from "./TodoItem.js";


class TodoList extends Component {
    render() {
      return (
        <section className="main">
          <ul className="todo-list">
            {this.props.todos.map(todo => (
              <TodoItem
                handleToggle={event => this.props.handleToggle(todo.id)}
                handleDelete={event => this.props.handleDelete(todo.id)}
                handleCompleted={event => this.props.handleCompleted(todo.id)}
                key={todo.id}
                id={todo.id}
                title={todo.title}
                completed={todo.completed}
              />
            ))}
          </ul>
        </section>
      );
    }
  }
  export default TodoList;